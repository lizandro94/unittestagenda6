# UnitTestAgenda6
***Generación de pruebas de interfaz de usuario***

Se han seguido las instrucciones de la `Guía 6` , se ha tomado el proyecto base y se han incorporado las clases según se indica en la guía, además se ha hecho uso del código en la carpeta `elements` para completar la funcionalidad. Todas las pruebas resultaron exitosas y el proyecto corrió correctamente.

Para ingresar a la calculadora web, después de correr el proyecto se usará la url: `https://localhost:5001/Calculator` 5001 se refiere al puerto en que se corre el proyecto, este puede variar.

**Integrantes**
 - David Bermúdez
 - Leonardo Flores
 - Leandro Fonseca
 - Jasser Gutiérrez